package com.guillaumevdn.gcore.lib.legacy_npc.navigation.pathfinding.movement;

/**
 * @author GuillaumeVDN
 */
public enum MovementType {

	OFFSET,
	OFFSET_STICK_TO_WALL,

	GET_IN_WATER,
	GET_OUT_OF_WATER,

	SWIMMING,  // FIXME todo
	LADDER;  // FIXME todo

}
