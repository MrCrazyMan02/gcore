package com.guillaumevdn.gcore.lib.element.editor;

/**
 * @author GuillaumeVDN
 */
public enum SlotPlacement {

	ANY,
	SKIP_ONE,
	START_ROW;

}
