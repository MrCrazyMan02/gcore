package com.guillaumevdn.gcore.lib.gui.element.item.type;

/**
 * @author GuillaumeVDN
 */
public enum IconNeed {

	REQUIRED,
	OPTIONAL,
	USELESS

}
