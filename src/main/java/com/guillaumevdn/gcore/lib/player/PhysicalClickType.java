package com.guillaumevdn.gcore.lib.player;

/**
 * @author GuillaumeVDN
 */
public enum PhysicalClickType {

	RIGHT_CLICK,
	LEFT_CLICK,
	RIGHT_CLICK_SNEAK,
	LEFT_CLICK_SNEAK,
	PHYSICAL;

}
